package com.jjr.spring_security.bookings.controllers;

import java.util.Arrays;
import java.util.List;

import com.jjr.spring_security.bookings.models.Booking;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/bookings")
public class BookingController {

    private static final List<Booking> BOOKINGS = Arrays.asList(
        new Booking(1L, "06/12/21", "James", "Bond"),
        new Booking(2L, "06/12/21", "Maria", "Jones"),
        new Booking(3L, "06/12/21", "Anna", "Smith")
    );

    @GetMapping(path = "{id}")
    public Booking getBooking(@PathVariable("id") Long bookingId) {

        return BOOKINGS.stream()
                .filter(booking -> bookingId.equals(booking.getId()))
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("Booking " + bookingId + " does not exist."));
    }
    
}
