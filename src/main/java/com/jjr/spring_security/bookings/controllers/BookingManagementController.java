package com.jjr.spring_security.bookings.controllers;

import java.util.Arrays;
import java.util.List;

import com.jjr.spring_security.bookings.models.Booking;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("management/api/v1/bookings")
public class BookingManagementController {
    
    private static final List<Booking> BOOKINGS = Arrays.asList(
        new Booking(1L, "06/12/21", "James", "Bond"),
        new Booking(2L, "06/12/21", "Maria", "Jones"),
        new Booking(3L, "06/12/21", "Anna", "Smith")
    );

    @GetMapping
    public List<Booking> getAllBookings() {
        System.out.println("getAllBookings");
        return BOOKINGS;
    }

    @PostMapping
    public void addBooking(@RequestBody Booking booking) {
        System.out.println("addBooking");
        System.out.println(booking);
    }

    @DeleteMapping(path = {"id"})
    public void deleteBooking(@PathVariable("id") Long bookingId) {
        System.out.println("deleteBooking");
        System.out.println(bookingId);
    }

    @PutMapping(path = {"id"})
    public void updateBooking(@PathVariable("id") Long bookingId, @RequestBody Booking booking) {
        System.out.println("updateBooking");
        System.out.println(String.format("%s %s", bookingId, booking));
    }


}
