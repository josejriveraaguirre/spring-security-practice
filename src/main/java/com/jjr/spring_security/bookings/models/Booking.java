package com.jjr.spring_security.bookings.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Booking {

    public Booking(Long id, String origEntryDate, String guestFirstName, String guestLastName) {
        this.id = id;
        this.origEntryDate = origEntryDate;
        this.guestFirstName = guestFirstName;
        this.guestLastName = guestLastName;
    }

    private Long id;

    private String origEntryDate;

    private String guestFirstName;

    private String guestLastName;

    private String guestEmail;

    private String guestCellphone;

    private Integer partyOf;
    
    private String locationRequested;

    private String arrivalDate;

    private String departureDate;

    private String status;

    private String lastModifiedDate;

    private String reviewedBy;

    private String lastReviewDate;

    @Override
    public String toString() {
        return "Booking [guestFirstName=" + guestFirstName + ", guestLastName=" + guestLastName + ", id=" + id + "]";
    }

    
    
}
