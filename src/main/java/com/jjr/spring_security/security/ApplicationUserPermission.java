package com.jjr.spring_security.security;

public enum ApplicationUserPermission {
    BOOKING_READ("booking:read"),
    BOOKING_WRITE("booking:write"),
    BOOKING_APPROVE("booking:approve");

    private final String permission;

    private ApplicationUserPermission(String permission) {
        this.permission = permission;
    }

    public String getPermission() {
        return permission;
    }

}
