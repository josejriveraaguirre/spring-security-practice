package com.jjr.spring_security.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import static com.jjr.spring_security.security.ApplicationUserRole.*;

@Configuration
@EnableWebSecurity
public class ApplicationSecurityConfig extends WebSecurityConfigurerAdapter{

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public ApplicationSecurityConfig(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .csrf().disable()
            .authorizeRequests()
            .antMatchers("/", "index", "/css/*", "/js/*").permitAll()
            .antMatchers("/api/**").hasRole(EMPLOYEE.name())
            .anyRequest().authenticated()
            .and()
            .httpBasic();
            
            // .antMatchers("/management/api/v1/bookings/*").hasRole(ADMIN.name())

    }

    @Override
    @Bean
    protected UserDetailsService userDetailsService() {
        UserDetails annaSmithUser = User.builder()
            .username("annasmith")
            .password(passwordEncoder.encode("password"))
            .roles(EMPLOYEE.name()) // ROLE_EMPLOYEE
            .build();

            UserDetails lindaUser = User.builder()
            .username("linda")
            .password(passwordEncoder.encode("password123"))
            .roles(ADMIN.name()) // ROLE_ADMIN
            .build();

            UserDetails tomUser = User.builder()
            .username("tom")
            .password(passwordEncoder.encode("password456"))
            .roles(ADMIN_TRAINEE.name()) // ROLE_ADMIN_TRAINEE
            .build();

        return new InMemoryUserDetailsManager(
            annaSmithUser,
            lindaUser,
            tomUser
        );


    }

}
