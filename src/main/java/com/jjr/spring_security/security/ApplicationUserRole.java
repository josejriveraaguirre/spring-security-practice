package com.jjr.spring_security.security;

import java.util.Set;

import com.google.common.collect.Sets;

import static com.jjr.spring_security.security.ApplicationUserPermission.*;

public enum ApplicationUserRole {
    EMPLOYEE(Sets.newHashSet()),
    ADMIN_TRAINEE(Sets.newHashSet(BOOKING_READ, BOOKING_WRITE)),
    ADMIN(Sets.newHashSet(BOOKING_READ, BOOKING_WRITE, BOOKING_APPROVE));

    private final Set<ApplicationUserPermission> permissions;

    private ApplicationUserRole(Set<ApplicationUserPermission> permissions) {
        this.permissions = permissions;
    }

    public Set<ApplicationUserPermission> getPermissions() {
        return permissions;
    }

    
}
